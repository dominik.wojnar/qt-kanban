#ifndef TASK_H
#define TASK_H
#include <QString>
#include <QJsonObject>
class Task
{
//    Q_GADGET;

public:
    Task();
    Task( const QString &id, const int state, const QString &name, const QString &assigned, const QString &description);
    QString name();
    void setName(const QString &name);

    QString assigned();
    void setAssigned(const QString &assigned);

    QString description();
    void setDescription(const QString &description);

    QString id();

    void setState(const int state);
    int state();

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
private:
    QString _name;
    QString _assigned;
    QString _description;
    QString _id;
    int _state;

};
#endif // TASK_H
