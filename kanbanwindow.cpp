#include "kanbanwindow.h"

KanbanWindow::KanbanWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KanbanWindow)
{
    ui->setupUi(this);
    taskManager = new TaskManager();
    draw();
}


KanbanWindow::~KanbanWindow()
{
    delete ui;
}

void KanbanWindow::draw(){
    QScrollArea* horizontalScrollArea = new QScrollArea();
    horizontalScrollArea->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    horizontalScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    horizontalScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    QWidget *area = new QWidget(horizontalScrollArea);
    QHBoxLayout* layout = new QHBoxLayout();
    for (int i = 0; i < 5; i++)
    {
        addColumn(layout, i);
    }
    area->setLayout(layout);
    horizontalScrollArea->setWidget(area);
    setCentralWidget(horizontalScrollArea);
}

void KanbanWindow::redraw(){
//    delete centralWidget();
    draw();
}

void KanbanWindow::addColumn(QHBoxLayout* horizontalScrollArea, int statusId){
    QVector<Task*>* tasks = taskManager->getTasksByStatus(statusId);

    QScrollArea* verticalScrollAreaLayout = new QScrollArea();
    verticalScrollAreaLayout->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    verticalScrollAreaLayout->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    verticalScrollAreaLayout->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    horizontalScrollArea->addWidget(verticalScrollAreaLayout, statusId);
    QWidget *area = new QWidget(verticalScrollAreaLayout);
    QVBoxLayout* layout = new QVBoxLayout(area);
    area->setLayout(layout);
    QLabel *statusNameLabel = new QLabel();

    QString l = taskManager->getStatusName(statusId);

    statusNameLabel->setText(l);
    layout->addWidget(statusNameLabel, 0);
    for (int i = 0; i < tasks->size(); i++)
    {
        addTask(layout, i, tasks->at(i));
    }
    verticalScrollAreaLayout->setWidget(area);
}

void KanbanWindow::addTask(QVBoxLayout* layout, int position, Task* task){
    TaskPreview* panelTaskPreview = new TaskPreview(this, taskManager);
    panelTaskPreview->setTask(task);
    panelTaskPreview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout->addWidget(panelTaskPreview, position);
}
