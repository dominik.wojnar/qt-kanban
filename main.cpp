#include "kanbanwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    KanbanWindow w;
    w.show();

    return a.exec();
}
