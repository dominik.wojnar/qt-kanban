#include "taskpreview.h"
#include "ui_taskpreview.h"
#include "taskedit.h"
#include "kanbanwindow.h"

TaskPreview::TaskPreview(QWidget *parent, TaskManager* taskManager) :
    QWidget(parent),
    ui(new Ui::TaskPreview)
{
    this->taskManager = taskManager;
    ui->setupUi(this);
    ui->labelName->setStyleSheet("font-weight: bold; color: #111111");
    ui->pushButtonDetails->setText("Details");
}

void TaskPreview::setTask(Task* task){
    currentTask = task;
    QString name = task->name();
    ui->labelName->setText(name);
    setWindowTitle(name);
    ui->labelAssigned->setText(task->assigned());
    ui->labelDescription->setText(task->description());
}

TaskPreview::~TaskPreview()
{
    delete ui;
}

void TaskPreview::on_pushButtonDetails_clicked()
{
    KanbanWindow* kanbanWindow = (KanbanWindow*)parent();
    TaskEdit *editor = new TaskEdit(this, kanbanWindow, taskManager);
    editor->setTask(currentTask);
    editor->show();
}
