#ifndef KANBANWINDOW_H
#define KANBANWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QVector>
#include <QPushButton>

#include "ui_kanbanwindow.h"
#include "taskpreview.h"
#include "taskmanager.h"
#include "task.h"

namespace Ui {
class KanbanWindow;
}

class KanbanWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit KanbanWindow(QWidget *parent = 0);
    ~KanbanWindow();
public slots:
    void redraw();

private:
    Ui::KanbanWindow *ui;
    void addTask(QVBoxLayout* layout, int position, Task* task);
    void addColumn(QHBoxLayout* verticalScrollAreaLayout, int position);
    void draw();
    TaskManager* taskManager;
};

#endif // KANBANWINDOW_H
